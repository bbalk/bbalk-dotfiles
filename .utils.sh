# who and where am i:
alias wwami='echo "$(whoami)@$(hostname):$(pwd)"'
export EDITOR='vim'


# activate a python virtualenv
function activate() {
    if [ ! -z "${VIRTUAL_ENV}" ]; then
      echo "Already active ${VIRTUAL_ENV}, unsetting VIRTUAL_ENV] ..."
      unset VIRTUAL_ENV
    fi

    dir="$(pwd)"
    name="$(basename ${dir})"
    external_venv="${HOME}/.virtualenvs/${name}"
    internal_venv="venv"
    if [ -d "${internal_venv}" ]; then
        venv="${internal_venv}"
    elif [ -d "${external_venv}" ]; then
        venv="${external_venv}"
    else
        echo "Could not find a virtualenv in ./${internal_venv} or ${external_venv}"
        return 1
    fi

    activate="${venv}/bin/activate"
    if [ -f "${activate}" ]; then
        source "${activate}" &&
            echo "Activated ${venv}"
    else
        echo "${activate} does not exist"
        return 1
    fi
}


function ls_or_page() {
    TARGET=${1:-'.'}

    if [ -d "${TARGET}" ]; then
        ls -la "${TARGET}"
    elif [ -f "${TARGET}" ]; then
        ${PAGER:-"cat"} "${TARGET}"
    else
        echo "ERROR: could not ls or ${PAGER:-'cat'} ${TARGET}"
    fi
}

alias c=ls_or_page


function shell_palette() {
    for i in {0..255}; do
	printf "\x1b[38;5;${i}m${i} "
    done
}

alias showp='shell_palette'

