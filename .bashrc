if [ -f ~/.bash_profile ]; then
  . ~/.bash_profile
fi
# BEGIN autocompletion for kubernetes
export BASH_COMPLETION_COMPAT_DIR="/usr/local/etc/bash_completion.d"
[[ -r "/Users/bbalk/brew/etc/profile.d/bash_completion.sh" ]] && . "/Users/bbalk/brew/etc/profile.d/bash_completion.sh"
source <(kubectl completion bash)
alias k=kubectl
complete -F __start_kubectl k
# END autocompletion for kubernetes
# BEGIN use latest bash
BASH_MAJOR_VER=`echo $BASH_VERSION|cut -d. -f1`
if [ "$BASH_MAJOR_VER" == "3" ]; then
 bash
 exit 0
fi 
# END use latest bash
# BEGIN kube-ps1 kubernetes
source "/Users/bbalk/brew/opt/kube-ps1/share/kube-ps1.sh"
PS1='$(kube_ps1)'$PS1
# END kube-ps1 kubernetes
# BEGIN python_fork_safety kubernetes
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
# END python_fork_safety kubernetes
