test -f "${HOME}/.utils.sh" && source "${HOME}/.utils.sh"

if [ -d "${HOME}/.oh-my-zsh" ]; then
    # Path to your oh-my-zsh installation.
    export ZSH=~/.oh-my-zsh

    ZSH_THEME="ben"
    ZSH_CUSTOM=${HOME}/.oh-my-zsh-custom

    # DISABLE_AUTO_UPDATE="true"
    COMPLETION_WAITING_DOTS="true"

    plugins=(aws git git-prompt python docker vi-mode zsh-autosuggestions)

    export ZSH_TMUX_ITERM2=false
    source $ZSH/oh-my-zsh.sh

    # set cursor to line when in insert mode
    VI_MODE_SET_CURSOR="true"

    # bind 'kj' to enter vi command mode
    bindkey kj vi-cmd-mode

    # https://github.com/robbyrussell/oh-my-zsh/issues/1720#issuecomment-286366959
    # start typing + [Up-Arrow] - fuzzy find history forward
    if [[ "${terminfo[kcuu1]}" != "" ]]; then
      autoload -U up-line-or-beginning-search
      zle -N up-line-or-beginning-search
      bindkey "${terminfo[kcuu1]}" up-line-or-beginning-search
    fi
    # start typing + [Down-Arrow] - fuzzy find history backward
    if [[ "${terminfo[kcud1]}" != "" ]]; then
      autoload -U down-line-or-beginning-search
      zle -N down-line-or-beginning-search
      bindkey "${terminfo[kcud1]}" down-line-or-beginning-search
    fi
else
    echo "${HOME}/.oh-my-zsh not found (https://github.com/robbyrussell/oh-my-zsh)"
fi

unsetopt share_history

# initialize pyenv
# export PYENV_ROOT=$HOME/.pyenv
# export PATH=$PYENV_ROOT/bin:$PATH

# if command -v pyenv 1>/dev/null 2>&1; then
#     eval "$(pyenv init -)"
# fi

# Default values for the prompt
# Override these values in ~/.zshrc or ~/.bashrc
KUBE_PS1_BINARY="${KUBE_PS1_BINARY:-kubectl}"
KUBE_PS1_SYMBOL_ENABLE="${KUBE_PS1_SYMBOL_ENABLE:-true}"
KUBE_PS1_SYMBOL_DEFAULT=${KUBE_PS1_SYMBOL_DEFAULT:-$'\u2388 '}
KUBE_PS1_SYMBOL_USE_IMG="${KUBE_PS1_SYMBOL_USE_IMG:-false}"
KUBE_PS1_NS_ENABLE="${KUBE_PS1_NS_ENABLE:-true}"
KUBE_PS1_PREFIX="${KUBE_PS1_PREFIX-(}"
KUBE_PS1_SEPARATOR="${KUBE_PS1_SEPARATOR-|}"
KUBE_PS1_DIVIDER="${KUBE_PS1_DIVIDER-:}"
KUBE_PS1_SUFFIX="${KUBE_PS1_SUFFIX-)}"
KUBE_PS1_SYMBOL_COLOR="${KUBE_PS1_SYMBOL_COLOR-blue}"
KUBE_PS1_CTX_COLOR="${KUBE_PS1_CTX_COLOR-magenta}"
KUBE_PS1_NS_COLOR="${KUBE_PS1_NS_COLOR-yellow}"
KUBE_PS1_BG_COLOR="${KUBE_PS1_BG_COLOR}"
KUBE_PS1_KUBECONFIG_CACHE="${KUBECONFIG}"
KUBE_PS1_DISABLE_PATH="${HOME}/.kube/kube-ps1/disabled"
KUBE_PS1_LAST_TIME=0
KUBE_PS1_CLUSTER_FUNCTION="${KUBE_PS1_CLUSTER_FUNCTION}"
KUBE_PS1_NAMESPACE_FUNCTION="${KUBE_PS1_NAMESPACE_FUNCTION}"

# BEGIN autocompletion for kubernetes
autoload -Uz compinit
compinit
source <(kubectl completion zsh)
alias k=kubectl
complete -F __start_kubectl k
# END autocompletion for kubernetes
# BEGIN python_fork_safety kubernetes
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
# END python_fork_safety kubernetes
# BEGIN export vault file
if [ -f /tmp/av ]; then
    export ANSIBLE_VAULT_PASSWORD_FILE=/tmp/av
fi
# END export vault file
alias ecr_login="aws --profile ecr sso login && aws --profile ecr ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 806860399886.dkr.ecr.us-east-1.amazonaws.com"
alias ecr_admin="aws --profile ecr-admin sso login && aws --profile ecr-admin ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 806860399886.dkr.ecr.us-east-1.amazonaws.com"
#secret-seal command aliased to docker run ...
alias secret-seal='docker run -it --rm --volume "$HOME/dev/skunk/secret-seal/public-key:/srv/public-key" -e SECRET_SEAL_NAMESPACE -e SECRET_SEAL_SERVICE -e SECRET_SEAL_CLUSTER_PUBLIC_KEY_PATH oncorps-secret-seal_app:latest'

# Adding env vars from keychain
export GIT_TOKEN=$(security find-generic-password -a "$USER" -s "LocalDevelopment_GIT_TOKEN" -w)
